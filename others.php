<?php

namespace NW\WebService\References\Operations\Notification;

use Exception;

/**
 * @property Seller $seller
 */
class Contractor
{
    public const TYPE_CUSTOMER = 0;

    public int $id;
    public int $type;
    public string $name;

    public static function getById(int $id): self
    {
        // fakes the getById method
        return new static($id); // заменено на static
    }

    public function getFullName(): string
    {
        return $this->name . ' ' . $this->id;
    }
}

class Seller extends Contractor
{
}

class Employee extends Contractor
{
}

class Client extends Contractor
{
    public string $email;
    public string $mobile;
}

class Status
{
    public static function getName(int $id): string
    {
        $a = [
            0 => 'Completed',
            1 => 'Pending',
            2 => 'Rejected',
        ];

        return $a[$id];
    }
}

abstract class ReferencesOperation
{
    abstract public function doOperation(): array;

    public function getRequest($pName): mixed
    {
        return $_REQUEST[$pName];
    }
}

class Helper
{
    public static function getResellerEmailFrom(int $resellerId): string
    {
        return 'contractor@example.com';
    }

    public static function getEmailsByPermit(int $resellerId, string $event): array
    {
        // fakes the method
        return ['someemeil@example.com', 'someemeil2@example.com'];
    }
}

class NotificationEvents
{
    const CHANGE_RETURN_STATUS = 'changeReturnStatus';
    const NEW_RETURN_STATUS    = 'newReturnStatus';
}

class DataReturnDTO
{
    public function __construct(
        public int $resellerId,
        public int $clientId,
        public int $creatorId,
        public int $expertId,
        public int $complaintId,
        public int $consumptionId,
        public int $differencesFrom,
        public int $differencesTo,
        public int $notificationType,
        public string $complaintNumber,
        public string $consumptionNumber,
        public string $agreementNumber,
        public string $date,
    ) {
    }
}

class View
{
    public static function render(string $template, ?array $data, int $id): string
    {
        // fakes the method
        return '';
    }
}

class MessagesClient
{
    public static function sendMessage(
        array $message,
        int $resellerId,
        string $status,
        int $clientId,
        int $differencesTo,
    ): void {
        // fakes the method
    }
}

class MessagesEmployee
{
    public static function sendMessage(
        array $message,
        int $resellerId,
        string $status,
    ): void {
        // fakes the method
    }
}

class MessageTypes
{
    public const EMAIL = 0;
}

class NotificationManager
{
    public static function send(
        int $resellerId,
        int $clientId,
        string $status,
        int $differencesTo,
        array $templateData,
        ?string &$error = null
    ): bool {
        // fakes the method
        return true;
    }

}

class NotFoundException extends Exception {}
