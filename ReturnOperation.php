<?php

namespace NW\WebService\References\Operations\Notification;

use Exception;
use InvalidArgumentException;
use LogicException;

class ReturnOperation extends ReferencesOperation
{
    private const  NOTIFICATION_TYPE_NEW    = 1;
    private const  NOTIFICATION_TYPE_CHANGE = 2;

    private Seller   $reseller;
    private Client   $client;
    private Employee $creator;
    private Employee $expert;

    /**
     * Отправка уведомлений о возврате товаров
     *
     * @return array
     * @throws NotFoundException
     * @throws InvalidArgumentException
     * @throws LogicException
     */
    public function doOperation(): array
    {
        $data = (array)$this->getRequest('data');
        $dataReturnDTO = new DataReturnDTO(
            (int)$data['resellerId'],
            (int)$data['clientId'],
            (int)$data['creatorId'],
            (int)$data['expertId'],
            (int)$data['complaintId'],
            (int)$data['consumptionId'],
            (int)$data['differences']['from'],
            (int)$data['differences']['to'],
            (int)$data['notificationType'],
            (string)$data['complaintNumber'],
            (string)$data['consumptionNumber'],
            (string)$data['agreementNumber'],
            (string)$data['date'],
        );

        $result = [
            'notificationEmployeeByEmail' => false,
            'notificationClientByEmail'   => false,
            'notificationClientBySms'     => [
                'isSent'  => false,
                'message' => '',
            ],
        ];

        if (empty($dataReturnDTO->resellerId)) {
            $result['notificationClientBySms']['message'] = 'Empty resellerId';
            return $result;
        }

        if (empty($dataReturnDTO->notificationType)) {
            throw new InvalidArgumentException('Empty notificationType', 400);
        }

        if (empty($dataReturnDTO->differencesFrom)) {
            throw new InvalidArgumentException('Empty differences from', 400);
        }

        if (empty($dataReturnDTO->differencesTo)) {
            throw new InvalidArgumentException('Empty differences to', 400);
        }

        $this->reseller = Seller::getById($dataReturnDTO->resellerId);
        if ($this->reseller === null) {
            throw new NotFoundException('Seller not found!', 400);
        }

        $this->client = Client::getById($dataReturnDTO->clientId);
        if ($this->client === null || $this->client->type !== Client::TYPE_CUSTOMER || $this->client->seller->id !== $this->reseller->id) {
            throw new NotFoundException('Client not found!', 400);
        }

        $this->creator = Employee::getById($dataReturnDTO->creatorId);
        if ($this->creator === null) {
            throw new NotFoundException('Creator not found!', 400);
        }

        $this->expert = Employee::getById($dataReturnDTO->expertId);
        if ($this->expert === null) {
            throw new NotFoundException('Expert not found!', 400);
        }

        // Формирование шаблона уведомлений в зависимости от типа операции
        if ($dataReturnDTO->notificationType === self::NOTIFICATION_TYPE_NEW) {
            $differences = View::render('NewPositionAdded', null, $this->reseller->id);
        } elseif ($dataReturnDTO->notificationType === self::NOTIFICATION_TYPE_CHANGE) {
            $differences = View::render('PositionStatusHasChanged', [
                'FROM' => Status::getName($dataReturnDTO->differencesFrom),
                'TO'   => Status::getName($dataReturnDTO->differencesTo),
            ], $this->reseller->id);
        } else {
            throw new InvalidArgumentException('Invalid notificationType', 400);
        }

        $templateData = [
            'COMPLAINT_ID'       => $dataReturnDTO->complaintId,
            'COMPLAINT_NUMBER'   => $dataReturnDTO->complaintNumber,
            'CREATOR_ID'         => $dataReturnDTO->creatorId,
            'CREATOR_NAME'       => $this->creator->getFullName(),
            'EXPERT_ID'          => $dataReturnDTO->expertId,
            'EXPERT_NAME'        => $this->expert->getFullName(),
            'CLIENT_ID'          => $dataReturnDTO->clientId,
            'CLIENT_NAME'        => $this->client->getFullName(),
            'CONSUMPTION_ID'     => $dataReturnDTO->consumptionId,
            'CONSUMPTION_NUMBER' => $dataReturnDTO->consumptionNumber,
            'AGREEMENT_NUMBER'   => $dataReturnDTO->agreementNumber,
            'DATE'               => $dataReturnDTO->date,
            'DIFFERENCES'        => $differences,
        ];

        // Если хоть одна переменная для шаблона не задана, то не отправляем уведомления
        foreach ($templateData as $key => $value) {
            if (empty($value)) {
                throw new LogicException("Template Data ({$key}) is empty!", 500);
            }
        }

        $emailFrom = Helper::getResellerEmailFrom($this->reseller->id);
        $result['notificationEmployeeByEmail'] = $this->sendEmployeeEmail($emailFrom, $templateData);

        // Шлём клиентское уведомление, только если произошла смена статуса
        if ($dataReturnDTO->notificationType === self::NOTIFICATION_TYPE_CHANGE ) {
            $result['notificationClientByEmail'] = $this->sendClientEmail($emailFrom, $templateData,
                $dataReturnDTO->differencesTo);
            if (!empty($this->client->mobile)) {
                $error = null;
                $result['notificationClientBySms']['isSent'] = $this->sendClientSms($templateData,
                    $dataReturnDTO->differencesTo, $error);

                if (!empty($error)) {
                    $result['notificationClientBySms']['message'] = $error;
                }
            }
        }

        return $result;
    }

    /**
     * Отправка email сообщений сотрудникам
     *
     * @param string $emailFrom E-mail отправителя
     * @param array $templateData Данные для шаблона
     * @return bool
     */
    private function sendEmployeeEmail(string $emailFrom, array $templateData): bool
    {
        // Получаем email сотрудников из настроек
        $emails = Helper::getEmailsByPermit($this->reseller->id, 'tsGoodsReturn');
        if (!empty($emailFrom) && count($emails) > 0) {
            foreach ($emails as $email) {
                MessagesEmployee::sendMessage([
                    MessageTypes::EMAIL => [
                        'emailFrom' => $emailFrom,
                        'emailTo' => $email,
                        'subject' => View::render('complaintEmployeeEmailSubject', $templateData, $this->reseller->id),
                        'message' => View::render('complaintEmployeeEmailBody', $templateData, $this->reseller->id),
                    ],
                ], $this->reseller->id, NotificationEvents::CHANGE_RETURN_STATUS);
            }

            return true;
        }

        return false;
    }

    /**
     * Отправка email сообщения клиенту
     *
     * @param string $emailFrom E-mail отправителя
     * @param array $templateData Данные для шаблона
     * @param int $differencesTo Код нового статуса
     * @return bool
     */
    private function sendClientEmail(string $emailFrom, array $templateData, int $differencesTo): bool
    {
        if (!empty($emailFrom) && !empty($this->client->email)) {
            MessagesClient::sendMessage([
                MessageTypes::EMAIL => [
                    'emailFrom' => $emailFrom,
                    'emailTo' => $this->client->email,
                    'subject' => View::render('complaintClientEmailSubject', $templateData, $this->reseller->id),
                    'message' => View::render('complaintClientEmailBody', $templateData, $this->reseller->id),
                ],
            ], $this->reseller->id, NotificationEvents::CHANGE_RETURN_STATUS, $this->client->id, $differencesTo);

            return true;
        }

        return false;
    }

    /**
     * Отправка SMS сообщения клиенту
     *
     * @param array $templateData Данные для шаблона
     * @param int $differencesTo Код нового статуса
     * @param string|null $error Возвращаемая ошибка
     * @return bool
     */
    private function sendClientSms(array $templateData, int $differencesTo, ?string &$error): bool
    {
        return NotificationManager::send(
            $this->reseller->id,
            $this->client->id,
            NotificationEvents::CHANGE_RETURN_STATUS,
            $differencesTo,
            $templateData,
            $error
        );
    }
}
